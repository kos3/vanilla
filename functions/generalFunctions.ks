// self explanatory
FUNCTION doSafeStage {
    WAIT UNTIL STAGE:READY.
    STAGE.
    WAIT 1.
}

// determines whether to stage
FUNCTION doAutoStage { 
    LIST ENGINES IN myEngines.
    FOR eg in myEngines {
        IF eg:IGNITION AND eg:FLAMEOUT {
            PRINT "FLAMEOUT".
            doSafeStage().
            BREAK.
        } 
    }
    IF SHIP:AVAILABLETHRUST < 0.1 {
        doSafeStage().
        PRINT "NO THRUST".
    }
}