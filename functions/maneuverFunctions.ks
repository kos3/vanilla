// executes the next available maneuver node or given node
FUNCTION executeNextNode {
    DECLARE LOCAL originalmnv TO NODE(TIME:SECONDS, 0, 0, 0).
    IF HASNODE = true {
        SET originalmnv TO NEXTNODE.
        LOCK currentmnv TO NEXTNODE.
    } ELSE {
        RETURN "NO NODE GIVEN".
    }
    
    DECLARE startTime TO calculateManeuverStartTime(originalmnv).
    lockSteeringToManeuverTarget(currentmnv).
    WAIT UNTIL startTime.
    WHEN vang(originalmnv:BURNVECTOR, SHIP:FACING:VECTOR) < 0.5 AND TIME:SECONDS > startTime -0.1 THEN {
        LOCK THROTTLE TO 1.   
    }

    UNTIL isManeuverComplete(originalmnv, currentmnv) {
        doAutoStage().
        adjustManeuverThrottle(currentmnv).
    }

    LOCK THROTTLE TO 0.
    REMOVE currentmnv.

    UNLOCK THROTTLE.
    UNLOCK STEERING.
}

// calculates the start time of a maneuver node in utime
FUNCTION calculateManeuverStartTime {
    PARAMETER mnv.
    RETURN TIME:SECONDS + mnv:ETA - calculateManeuverBurnTime(mnv) / 2.
}

// calculates burn time of a maneuver node
FUNCTION calculateManeuverBurnTime {
    PARAMETER mnv.
    DECLARE LOCAL dV TO mnv:DELTAV:MAG.
    DECLARE LOCAL g0 TO 9.8055.
    DECLARE LOCAL isp TO 0.

    LIST ENGINES IN myEngines.
    FOR eng IN myEngines {
        IF eng:IGNITION AND NOT eng:FLAMEOUT {
            SET isp TO isp + (eng:ISP * (eng:AVAILABLETHRUST / SHIP:AVAILABLETHRUST)).
        }
    }

    DECLARE LOCAL mf TO SHIP:MASS / constant():e^(dV / (isp * g0)).
    DECLARE LOCAL fuelFlow TO SHIP:AVAILABLETHRUST / (isp * g0).
    DECLARE LOCAL dt TO (SHIP:MASS - mf) / fuelFlow.

    RETURN dt.
}

// points ship at manuever node
FUNCTION lockSteeringToManeuverTarget {
    PARAMETER mnv.
    LOCK STEERING TO mnv:BURNVECTOR.
}

// determines whether or not a maneuver node is complete
FUNCTION isManeuverComplete {
    PARAMETER originalmnv, currentmnv.
    IF vang(originalmnv:BURNVECTOR, currentmnv:BURNVECTOR) > 90 OR currentmnv:DELTAV:MAG < 0.2 {
        RETURN true.
    } ELSE {
        RETURN false.
    }
}

// adjusts throttle during maneuver burn
FUNCTION adjustManeuverThrottle {
    PARAMETER mnv.
    IF mnv:DELTAV:MAG < 10 {
        LOCK THROTTLE TO mnv:DELTAV:MAG / 10.
    }
}