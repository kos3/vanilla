// kOS is so hard 
// oh well let's try to write a script anyways

@LAZYGLOBAL OFF.
CLEARSCREEN.

GLOBAL targetHeading IS 90.

FUNCTION main 
{
    SET STEERINGMANAGER:MAXSTOPPINGTIME TO 6.
    LOCAL desiredApoapsis IS 75000.
    // DECLARE LOCAL initialTWR TO SHIP:MAXTHRUST / SHIP:MASS. // maybe implement some TWR stuff later.

    startup().

    ascent(desiredApoapsis).
    coast(20).
    circularize().

    shutdownSequence().
}

FUNCTION startup
{
    LOCK THROTTLE TO 1.
    LOCK STEERING TO HEADING(targetHeading, 90, 0).

    countdown(3).
    safeStage().
    WAIT 1.
    CLEARSCREEN.
}

FUNCTION ascent
{
    PARAMETER desiredApoapsis.

    LOCAL myDir IS HEADING(targetHeading, 90).
    LOCAL pitch IS 0.
    LOCK STEERING TO myDir.

    UNTIL SHIP:APOAPSIS > desiredApoapsis {
        SET pitch to 90 * (1 - (SHIP:ALTITUDE / BODY:ATM:HEIGHT) ^ 0.5) + 2.5. // some trajectory I found online, can be tweaked.
        SET myDir TO HEADING(targetHeading, pitch).
        safeStage().
    }

    LOCK THROTTLE TO 0.
    WAIT 0.5.
}

FUNCTION coast 
{
    PARAMETER tta.

    LOCK STEERING TO HEADING(targetHeading, 0).

    IF ABS(SHIP:FACING:PITCH) < 0.1 AND ETA:APOAPSIS > tta + 10 AND SHIP:ALTITUDE < BODY:ATM:HEIGHT {
        SET KUNIVERSE:TIMEWARP:MODE TO "PHYSICS".
        WAIT 0.1.
        SET WARP TO 3.
    }

    UNTIL ETA:APOAPSIS < tta + 10 { } // wait until burn time.

    SET WARP TO 0.
    deployFairings().
    WAIT 10.
}

FUNCTION circularize
{
    LOCAL tta IS ETA:APOAPSIS.
    LOCAL pitch IS 0.
    LOCAL maxPitch IS 25.

    LOCK STEERING TO HEADING(targetHeading, pitch).

    UNTIL ABS(SHIP:VELOCITY:ORBIT:MAG - circOrbitVelocity(SHIP:APOAPSIS)) < 0.5 {
        IF ETA:APOAPSIS < tta OR SHIP:VERTICALSPEED < 0 {
            LOCK THROTTLE TO 1.0.
            IF SHIP:VERTICALSPEED < 0 {
                SET pitch TO maxPitch.
            } ELSE {
                SET pitch TO maxPitch * (1.0 - ETA:APOAPSIS / tta).
            }            
        } ELSE {
            SET pitch TO MAX(-1.0 * maxPitch * (ETA:APOAPSIS - tta) / tta, -maxPitch).
            LOCK THROTTLE TO MAX(1.0 - (ETA:APOAPSIS - tta) / tta, 0).
        }

        safeStage().
    }
    LOCK THROTTLE TO 0.
}

FUNCTION shutdownSequence
{
    PRINT "Shutting Down...".
    LOCK THROTTLE TO 0.
    UNLOCK THROTTLE.
    UNLOCK STEERING.
    // SAS ON.

    IF SHIP:ALTITUDE > BODY:ATM:HEIGHT {
        PANELS ON.
        LIGHTS ON.
    }
}

// ===== COMMONLY USED FUNCTIONS ===== //

FUNCTION safeStage // found this online, should stage with any rocket.
{
    IF MAXTHRUST = 0 {
        IF STAGE:READY {
            PRINT "No engines, staging.".
            STAGE.
        }
    }

    LOCAL engines IS List().
    LOCAL numFO IS 0.
    LIST ENGINES IN engines.
    FOR eng IN engines {
        IF eng:FLAMEOUT {
            SET numFO TO numFO + 1. 
        }
    }

    IF numFO > 0 {
        IF STAGE:READY {
            IF numFO = 1 {
                PRINT "1 Engine Flameout".
                STAGE.
            } ELSE {
                PRINT numFO + " Engine Flameouts".
                STAGE.
            }
        }
    }
}

FUNCTION countdown
{
    PARAMETER len.

    PRINT "Beginning Launch Sequence:".
    PRINT "Launching in...".
    WAIT 1.

    FROM {len.} UNTIL len = 0 STEP {SET len TO len - 1.} DO {
        PRINT "..." + len.
        WAIT 1.
    }
}

FUNCTION circOrbitVelocity
{
    PARAMETER height.

    RETURN SQRT(BODY:MU / (height + BODY:RADIUS)).
}

FUNCTION deployFairings
{
    FOR f IN SHIP:MODULESNAMED("ModuleProceduralFairing") { 
        f:DOEVENT("deploy"). 
    }
}



// run main
main().
