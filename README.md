I do not really know how to write a good read me.

I have not touched most of the files in here in a really long time.

/-------------------------------------------------------------------------------/

launch.ks is the most up to date file for realtively straight forward launches.
It uses a desired apoapsis and heading to get into a circular orbit. Currently,
it does not work with very large apogees, and I have not tried 
retrograde orbits but I do not see any issues with that.

general2.ks is my old launch script that is very long and does not get into as 
circular an orbit as launch.ks does. It does have some maneuver node functions 
however, that might be able to be scrapped and polished (aka fixed).

The other files I believe were just derivations of general2.ks for specific
rockets I was using, but launch.ks is the preferred launch script now.