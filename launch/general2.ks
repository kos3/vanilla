// trying out some kOS.
// TO DO: add info readouts to the terminal

// include required common functions
RUNPATH("0:/functions/maneuverFunctions.ks").
RUNPATH("0:/functions/generalFunctions.ks").

// clear terminal to look fancy
CLEARSCREEN.

// main function
FUNCTION main {
    SET STEERINGMANAGER:MAXSTOPPINGTIME TO 6. // depends on craft, more = longer turns
    DECLARE LOCAL desiredApoapsis TO 100000.
    DECLARE LOCAL desiredPeriapsis TO 100000.

    launchSequence(desiredApoapsis).
    createFinalOrbitNode(desiredApoapsis, desiredPeriapsis).
    executeNextNode().
    shutdownSequence().
}

// --- BEGINNING OF LAUNCH SEQUENCE --- //

// launches rocket until desired apoapsis is achieved
FUNCTION launchSequence {
    PARAMETER targetApoapsis.

    startupSequence().
    UNTIL SHIP:APOAPSIS > targetApoapsis {
        updateHeading(). // steepness factor?
        checkFairing().
        doAutoStage().
    }
    LOCK THROTTLE TO 0. WAIT 1.
}

// start countdown sequence and launch
FUNCTION startupSequence {
    PRINT "Counting down:".
    WAIT 1.
    FROM {LOCAL countdown IS 3.} UNTIL countdown = 0
        STEP {SET countdown TO countdown - 1.} DO {
            PRINT "..." + countdown.
            WAIT 1.
        }
    LOCK THROTTLE TO 1.
    STAGE.
    PRINT "LIFTOFF.".
    WAIT 1.
    CLEARSCREEN.
}

// determines the current heading of the rocket based on altitude (flight profile)
FUNCTION updateHeading {
    DECLARE headingx TO 90.0. DECLARE headingy TO 90.0.
    DECLARE finalheadingy TO 10.0. DECLARE initialalt TO 500. DECLARE finalalt TO 50000.

    IF SHIP:ALTITUDE > finalalt {
        SET headingy TO finalheadingy.
    } ELSE IF SHIP:ALTITUDE > initialalt {
        SET headingy TO 90.0 - (SHIP:ALTITUDE - initialalt) * (90 - finalheadingy) / (finalalt - initialalt).
    }

    LOCK STEERING TO HEADING(headingx, headingy, 0.0).
}

FUNCTION checkFairing {
    IF SHIP:ALTITUDE > BODY:ATM:HEIGHT * 0.75 {
        AG10 ON.
    }
}

// unlocks controls and deploys panels once apoapsis is achieved
FUNCTION shutdownSequence { 
    TOGGLE PANELS.
    UNLOCK STEERING.
    UNLOCK THROTTLE.
    SAS ON.

    WAIT 1.
}

// --- END OF LAUNCH SEQUENCE FUNCTIONS --- //
// --------------------------------------- //
// --- BEGINNING OF CIRCULARIZE FUNCTION --- //

// creates maneuver to circularize orbit (for launch)
FUNCTION createFinalOrbitNode {
    PARAMETER ap. PARAMETER pe.
    DECLARE mu TO BODY:MU.
    DECLARE r TO BODY:RADIUS.
    DECLARE LOCAL dV TO sqrt(-2 * mu / (ap + pe + 2 * r) + 2 * mu / (SHIP:ORBIT:APOAPSIS + r)) -
                        sqrt(-mu / SHIP:ORBIT:SEMIMAJORAXIS + 2 * mu / (SHIP:ORBIT:APOAPSIS + r)).
    ADD NODE(TIME:SECONDS + ETA:APOAPSIS, 0, 0, dV).
}

// --- END OF MANEUVER FUNCTIONS --- //
// ------------------------------------ //

// run the program
main().